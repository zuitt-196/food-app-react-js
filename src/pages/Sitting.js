import React, {useState,useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';


const Sitting = () => {
  //define default color
  const [theme ,setTheme]=useState("light");


// define define useState initilization with some backgraound color and text
const [setting, setSetting] = useState({
  "--background-color" : "#fff",
  "--background-light" :"#fff",
  "--primary-color": "rgb(255, 0, 86)",
  "--shadow-color" : "rgba(0,0,0,0.2)",
  "--text-color": "#0A0A0A",
  "--text-light": "#57575",
  "--font-size": "16px",
  "--animation-speed":1
})  




// define to update our DOM
useEffect(() => {
  // console.log("updating setting")
  const root = document.documentElement;
   for(let key in setting){
    const localroot =  root.style.setProperty(key, setting[key]);

     
   }
   
}, [setting])

//define thems array of object 
   const themes = [
    {
      "--background-color" : "#fff",
     "--background-light" :"#fff",
    "--primary-color": "rgb(255 0, 86)",
    "--shadow-color" : "rgba(0,0,0,0.2)",
    "--text-color": "#0A0A0A",
    "--text-light": "#57575",
    },
    {
    "--background-color" : "rgb(29,29,29)",
    "--background-light" :"rgb(77,77,77)",
    "--primary-color": "rgb(255 0, 86)",
     "--shadow-color" : "rgba(0,0,0,0.2)",
     "--text-color": "#ffffff",
     "--text-light": "#ececea",
    }
   ] 

   //difine function for changes
   const changeTheme = (i)=>{
      const _theme = {...themes[i]};
      setTheme(i === 0 ? "light" : "dark");
      // update settings
      let _settings = {...setting}; 
        for(let key in _theme) {
          console.log(key);
          _settings[key] =  _theme[key]

        } 

        setSetting(_settings)
     
    
   }

   const  chnageColor = (i) =>{
    // preventDefault() 
    const _color = primaryColors[i]
    let _setting = {...setting} 
    _setting[ "--primary-color"] = _color 
    setPrimaryColor(i)
    setSetting(_setting)
   }


   const chnagesFontSize = (i) => {
      const _size = fontSizes[i]
      let _setting = {...setting}
      _setting["--font-size"] = _size.value
      setFontSize(i);
      setSetting(_setting);

   }
   const chageAnimationSpeed = (i) => {
      let _speed = animationSpeeds[i]
      let _setting = {...setting}
      _setting[ "--animation-speed"] = _speed.value
      setAnimatiSpeeds(i)
      setSetting(_setting)
   }

  
  // define the primary color with the default values of index
  const [primaryColor, setPrimaryColor] = useState(0)
  
  //defifne the list array of primarycolor
  const primaryColors = [
      "rgb(255, 0, 86",
      "rgb(33, 150, 243",
      "rgb(255, 183, 87",
      "rgb(0, 200, 83",
      "rgb(156, 39, 176",
  ]



  // define the primary font size the default values of index
    const [fontSize, setFontSize] = useState(2)


// define the list of font-size
  const fontSizes = [
    {
      title: "Small",
      value: "12px"

    
    }, {
      title: "Medium",
      value: "16px"

    
    },
    {
      title: "Large",
      value: "20px"

    
    }

]

//define the default values index in animation speeds 
const [animatiSpeeds, setAnimatiSpeeds] = useState(1)
const animationSpeeds = [
  {
    title: "Slow",
    value: 2
  },
  {
    title: "Midium",
    value: 1
  },
  {
    title: "Fast",
    value: .5
  }
]


  return (
    <>
    <div className="section d-block">
      <h2>Primary Theme</h2>
      <div className='option-container'>
            <div className="option light" onClick={() => changeTheme(0)}>
              {theme === "light" && (
                    <div className="check">
                    <FontAwesomeIcon icon={ faCheck } />
                  </div>
              )}
            </div>
            <div className="option dark" onClick={() =>changeTheme(1)}>
            {theme === "dark" && (
                    <div className="check">
                    <FontAwesomeIcon icon={ faCheck } />
                  </div>
              )}
            </div>  
      </div>
    </div>
     
    <div className="section d-block">
      <h2>Preffered color</h2>
      <div className='option-container' >
              { primaryColors.map((color, index) =>(
                  <div key={index} className="option light" style={{backgroundColor: color}} onClick={()=> chnageColor(index)}>
                      { primaryColor === index && (
                        <div className="check">
                        <FontAwesomeIcon icon={ faCheck } />
                         </div>
                      )}
                  </div>
                  
              ))}
            
            </div>  
      </div> 
      
      <div className="section d-block">
          <h2>Font size</h2>
          <div className='option-container'>
                  {fontSizes.map((font, index) =>(
                  <button key={index} className="btn" onClick={() =>chnagesFontSize(index)}>
                  {font.title} 
                    {fontSize === index && (
                                <span>  <FontAwesomeIcon icon={faCheck }/></span>
                    )}
                  </button>
                  ))}
                </div>  
      </div>

      <div className="section d-block">
          <h2>Animation speed</h2>
          <div className='option-container'>
                  {animationSpeeds.map((speed, index) =>(
                  <button key={index}className="btn" onClick={() =>  chageAnimationSpeed(index)}>
                  {speed.title} 
                    {animatiSpeeds === index && (
                                <span>  <FontAwesomeIcon icon={faCheck }/></span>
                    )}
                  </button>
                  ))}
                </div>  
      </div>

    
    </>
  )
}

export default Sitting