import React from 'react'
import HeroSection from '../components/HeroSection';
import QuoteSection from '../components/QuoteSection';
import ChiefsSection from '../components/ChiefsSection'
import ImproveSkills from '../components/ImproveSkills';

const Home = () => {
  return (
    <div>
    <HeroSection/>
      <ImproveSkills />
      <QuoteSection />
      <ChiefsSection />
 
    </div>
  )
}

export default Home