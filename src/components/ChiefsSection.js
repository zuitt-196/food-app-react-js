import React from 'react'
import ChiefsCards  from './ChiefsCards'

const ChiefsSection = () => {
    const chiefs = [
        {
            name: "Vhong Bercasio",
            img: "/img/top-chiefs/img_1.jpg",
            recipesCount: "10",
            cuisine: "Mexican"
        },
        {
            name: "Blair Buntas",
            img: "/img/top-chiefs/img_2.jpg",
            recipesCount: "05",
            cuisine: "Japanese"
        },
        {
            name: "Rio boy",
            img: "/img/top-chiefs/img_3.jpg",
            recipesCount: "18",
            cuisine: "Italain"
        },
        {
            name: "Jonh Thunder",
            img: "/img/top-chiefs/img_4.jpg",
            recipesCount: "12",
            cuisine: "Chinese"
        },
        {
            name: "Edgar Mainit",
            img: "/img/top-chiefs/img_5.jpg",
            recipesCount: "12",
            cuisine: "American"
        },
        {
            name: "Jayve Ubos",
            img: "/img/top-chiefs/img_6.jpg",
            recipesCount: "17",
            cuisine: "Indian"
        }

    ]
  return (
    <div className="section chiefs">
        <h1 className='title'>Our Top Chiefs</h1>
        <div className="top-chiefs-container">
                {chiefs.map((chief)=> (
                    < ChiefsCards key={chief.name} chief={chief}/>
                ))}
        
        </div>
    </div>
  )
}

export default ChiefsSection