import React from 'react'
import { FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faQuoteLeft} from "@fortawesome/free-solid-svg-icons";


const QuoteSection = () => {
  return (
    <div className="section quote">
            <p className="quote-text"> <FontAwesomeIcon  icon={faQuoteLeft}/> Eating is so intimate. It's very sensual. When you invite someone to sit at your table and you want to cook for them, you're inviting a person into your lifet </p>
            <p className="quote-auther"> - Maya Angeluo</p>
        </div>
  )
}

export default QuoteSection