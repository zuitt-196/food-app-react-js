import React from 'react'

const Footer = () => {
  return (
    <div className='footer container'>
        <div className="footer-section">
                <p className="title">
                        FoodiesHub.com
                </p>
                <p>FoodiesHub.com is place where you can place soul and tummy with delicious food recipes  of all cuisine And our service i a absolutely</p>
                <p>&copy; 2022 || All Rigt Reserve</p>
        </div>
        <div className=" footer-section">
                <p className="title">
                        Contact Us
                </p>
                <p>foodiehhub@gmail.com</p>
                <p>+342-5324-94254</p>
                <p>2393 Bislig city</p>

        </div>
        <div className=" footer-section">
                <p className="title">
                   Social   
                </p>
                    <p>Facebook</p>
                    <p>Twitter</p>
                    <p>Instagram</p>
        </div>
    </div>
  )
}

export default Footer
