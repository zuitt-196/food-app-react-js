import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faSearch} from "@fortawesome/free-solid-svg-icons";


const Previous = () => {
      // define the array object for previous search as static
  const searches= [
    "pizza",
    "burger",
      "cokies",
      "juice",
      "briyani",
      "salad",
      "ice cream",
      "lasagan",
      "soup"
]

  return (
    <div>
      <div className="preview-search section">
        <h2> Previous Search</h2>
      <div className="prev-search-container"> 
       {searches.map((search, index) => (
         <div key={index} style={{animationDelay: index * .2 + "s"}}className="search-item"> {search} </div>
         ))}
      </div>
     
    <div className="search">
        <input type="text"  placeholder="Search...."/>
          <button className='btn'>
            < FontAwesomeIcon icon={faSearch} />
          </button>

    </div>
    </div>
    </div>
  )
}

export default Previous
