import React from 'react'
import {Link, useLocation} from 'react-router-dom'

// import fortawesome 
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


const SideBar = ({links, close}) => {
  // define location as navigation of sidebar
  const location = useLocation();

  return (
    <div className="sidebar"  onClick={close}>
            {links.map(link =>(
                <Link  to={link.path} className= {location.pathname === link.path ? "sidebar-link active" : "sidebar-link" } key={link.name}>
                  <FontAwesomeIcon icon={link.icon} />
                  {link.name}
                  </Link>
            ))}
    </div>
  )
}

export default SideBar