
import React, {useState} from 'react'
import {Link, useLocation } from 'react-router-dom'

//import icon used of fontawsome [SECTION]
import {faHome, faList, faCog} from "@fortawesome/free-solid-svg-icons";

// import components[SECTION]
import SideBar from './SideBar'

const Navbar = () => {
  //define the uselocation for as navigation 
  const location = useLocation();

    // define usestae for showinf the menubar
    const [showsideBar, setShowsideBar] = useState(false);

    // define the links 
    const links = [
      {
        name: "Home",
        path: "/",
        icon: faHome
      },
      {
        name: "Recipes",
        path: "/recipes",
        icon:faList
      },
      {
        name: "Settings",
        path: "/sittings",
        icon:faList
      },


       
    ]

    // define the close sidebar

    const closeSidebar = () => {
        setShowsideBar(false)
    }

  return (
    <>
    <div className="navbar container">
    <Link to='/' className="logo">F<span>ood</span>iedHub</Link>
    <div className="nav-links">
         {links.map((link) =>(
            <Link to={link.path} key={link.name} className={location.pathname === link.path ? 'active' : " "}>{link.name}</Link>
         ))}
    </div>
    <div onClick={() => setShowsideBar(true)} className={showsideBar ? " sidebar-btn active": " sidebar-btn"}>
            <div className="bar "></div>
            <div className="bar"></div>
            <div className="bar"></div>
    </div>
    </div>
    {showsideBar &&  <SideBar links={links} close={closeSidebar}/>}

    </>
  )
}

export default Navbar
