import React from 'react'
import { BrowserRouter as Router , Routes, Route } from 'react-router-dom';

// components [SECTIONS]
import Navbar from './components/Navbar'
import Footer from './components/Footer'

// IMPORT PAGES [SECTION]
import Home from './pages/Home'
import Recipes from './pages/Recipes'
import Sitting from './pages/Sitting'



function App() {
  return (
    <Router>
    <Navbar/>
    <div className="container main">
      <Routes>
        <Route  path='/' element={<Home/>} />
        <Route  path='/recipes' element={<Recipes/>} />
        <Route  path='/sittings' element={<Sitting/>} />
      </Routes>
    </div>
    <Footer />
    </Router>
  )
}

export default App
